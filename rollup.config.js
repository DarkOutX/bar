import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload'
import scss from 'rollup-plugin-scss';
import svgSprite from 'rollup-plugin-svg-sprite';
import babel from 'rollup-plugin-babel';


// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;

export default {
	input: 'src/main.js',
	output: {
		file: 'build/assets/bundle.js',
		format: 'iife', // immediately-invoked function expression — suitable for <script> tags
		sourcemap: false
	},
	plugins: [
		svgSprite({
			outputFolder: 'build/assets'
		}),
		scss({
		  output: 'build/assets/bundle.css',
		  outputStyle: "compressed",
		}),
		babel(),
		!production && livereload(),
		!production && serve({
			contentBase: ['build'],
			host: 'localhost',
			port: 3000,
		}),
		resolve(), // tells Rollup how to find date-fns in node_modules
		commonjs(), // converts date-fns to ES modules
		production && terser() // minify, but only in production
	]
};
