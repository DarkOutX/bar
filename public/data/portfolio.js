export default {
    ru: [
        {
            name: "Figma-Parser",
            img: "",
            description: "Класс для автоматизации работы с web-сервисом Figma.com. Позволяет создавать и использовать пресеты для пакетного экспорта слоёв в формате *.svg в указанные папки с заданными именами.",
            links: {
                "git": "https://gitlab.com/DarkOutX/figmaparser",
            }
        },{
            name: "FIASCO PLAYFIELD",
            img: "fiasco.jpg",
            description: "Поле для разговорной настольной игры Фиаско. Посреди стола лежат игральные кубики, а игроки в ходе партии их разбирают.",
            links: {
                "git": "https://gitlab.com/DarkOutX/fiasco-playfield",
            }
        }, {
            other: true,
            name: "Bang! Death Mesa",
            img: "bang.jpg",
            description: "Перевод с английского и верстка буклета правил и карт дополнения к настольной игре Бэнг!",
            links: {
                "git": "https://gitlab.com/DarkOutX/fiasco-playfield",
            }
        },
    ],
    en: [
        {
            name: "Figma-Parser",
            img: "",
            description: "Класс для автоматизации работы с web-сервисом Figma.com. Позволяет создавать и использовать пресеты для пакетного экспорта слоёв в формате *.svg в указанные папки с заданными именами.",
            links: {
                "git": "https://gitlab.com/DarkOutX/figmaparser",
            }
        },{
            name: "FIASCO PLAYFIELD",
            img: "fiasco.jpg",
            description: "Поле для разговорной настольной игры Фиаско. Посреди стола лежат игральные кубики, а игроки в ходе партии их разбирают.",
            links: {
                "git": "https://gitlab.com/DarkOutX/fiasco-playfield",
            }
        }, {
            other: true,
            name: "Bang! Death Mesa",
            img: "bang.jpg",
            description: "Перевод с английского и верстка буклета правил и карт дополнения к настольной игре Бэнг!",
            links: {
                "git": "https://gitlab.com/DarkOutX/fiasco-playfield",
            }
        },
    ]
}