export default {
    "ru": {
        "doc_title": "ZhukovD - Жуков Дмитрий, WEB-разработка, SEO-продвижение.",
        "next_lang": "English Russian",
        //MAIN
        "name": "Дмитрий Жуков",
        "img_title": "Дмитрий Жуков, WEB-разработчик",
        "img_alt": "Дмитрий Жуков фото",
        "web_developer": "Web Разработчик",

        //HOME
        "about_me": "Обо мне",
            "age": "21 год",
            "ref_long": "Более 6 лет связан с WEB-разработкой. Наиболее интересные направления:</br> визуальные эффекты, 3D в браузере, автоматизация процессов и обработка данных.",
        "languages": "Языки",
            "russian": "Русский",
            "english": "Английский",
            "spanish": "Испанский",
        "biography": "Биография",
            "biography_short": "(краткая)",
            "biography_full": "(подробная)",
            
        //SKILLS
        "skills": "Навыки",
        //skills-seo
        "seo_audit": "Аудит сайта",
        "seo_optimisation": "SEO-оптимизация сайта",
        "seo_ya_metrika": "Яндекс.Метрика",
        "seo_ya_direct": "Яндекс.Директ",
        "seo_google_analytics": "Google Аналитика",
        "seo_google_ad": "Google Реклама",
        //skills-admin
        "admin_linux": "Установка Linux-серверов",
        "admin_web": "Настройка WEB-сервера",
        "admin_remote": "Удаленный доступ",
        "admin_vpn": "Настройка приватной сети (VPN)",
        
        //PORTFOLIO
        "portfolio": "Портфолио",
        //descriptions
        "desc_figma":"Класс для автоматизации работы с web-сервисом Figma.com. Позволяет создавать и использовать пресеты для пакетного экспорта слоёв в формате *.svg в указанные папки с заданными именами.",
        "desc_fiasco":"Поле для разговорной настольной игры Фиаско. Посреди стола лежат игральные кубики, а игроки в ходе партии их разбирают.",
        "desc_bang":"Перевод с английского и верстка буклета правил и карт дополнения к настольной игре Бэнг!",
            "rules":"Правила",
            "cards":"Карты",
    },
    "en": {
        "doc_title": "ZhukovD - Dmitry Zhukov, Web Development, SEO Optimisation.",
        "next_lang": "English Russian",
        //MAIN
        "name": "Dmitry Zhukov",
        "img_title": "Dmitry Zhukov, Web Developer",
        "img_alt": "Dmitry Zhukov personal photo",
        "web_developer": "Web Developer",

        //HOME
        "about_me": "About me",
            "age": "21yo",
            "ref_long": "Более 6 лет связан с WEB-разработкой. Наиболее интересные направления:</br> визуальные эффекты, 3D в браузере, автоматизация процессов и обработка данных.",
        "languages": "Languages",
            "russian": "Russian",
            "english": "English",
            "spanish": "Spanish",
        "biography": "Biography",
            "biography_short": "(short)",
            "biography_full": "(full)",
            
        //SKILLS
        "skills": "Skills",
        //skills-seo
        "seo_audit": "SEO Audit",
        "seo_optimisation": "SEO Optimisation",
        "seo_ya_metrika": "Yandex.Metrika",
        "seo_ya_direct": "Yandex.Direct",
        "seo_google_analytics": "Google Analytics",
        "seo_google_ad": "Google Ads",
        //skills-admin
        "admin_linux": "Setting up Linux server",
        "admin_web": "Setting up WEB-server",
        "admin_remote": "Remote access",
        "admin_vpn": "Setting up VPN",
        
        //PORTFOLIO
        "portfolio": "Portfolio",
        //descriptions
        "desc_figma":"Класс для автоматизации работы с web-сервисом Figma.com. Позволяет создавать и использовать пресеты для пакетного экспорта слоёв в формате *.svg в указанные папки с заданными именами.",
        "desc_fiasco":"Поле для разговорной настольной игры Фиаско. Посреди стола лежат игральные кубики, а игроки в ходе партии их разбирают.",
        "desc_bang":"Перевод с английского и верстка буклета правил и карт дополнения к настольной игре Бэнг!",
            "rules":"Rulebook",
            "cards":"Cards",
    }
}