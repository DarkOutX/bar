export default {
    ru: [
        {
            date: "2020",
            title: "Svelte, React",
            content: "Начал использовать реактивные Frontend-фреймворки.",
        }, {
            date: "2019",
            title: "Node.js",
            content: "Вместо PHP в серверной части начал использовать JavaScript.",
        }, {
            date: "2018",
            title: "Начало работы с JavaScript",
            content: "Выполнял небольшие заказы, используя jQuery.",
        }, {
            date: "2017-2018",
            title: "Backend 1C-Bitrix",
            content: "Прошел сертификацию, проработал около семи месяцев и потерял интерес к 1С-Bitrix.",
        }, {
            date: "2017",
            title: "SEO",
            content: "На фрилансе выполнял заказы по SEO-оптимизации.",
        }, {
            date: "2016-2017",
            title: "Системное администрирование",
            content: "Паралелльно с выполнением заказов по PHP, работал помощником системного администратора.",
        }, {
            date: "2014",
            title: "Начало работы с PHP",
            content: "Выполнял небольшие заказы, создавал админ-панели, формы обратной связи и т.п.",
        },
    ],
    en: [
        {
            date: "2020",
            title: "Svelte, React",
            content: "Began to use reactive Frontend-frameworks.",
        }, {
            date: "2019",
            title: "Node.js",
            content: "Began to use Javascript instead of PHP on server side.",
        }, {
            date: "2018",
            title: "Began to study JavaScript",
            content: "Was creating some projects using jQuery.",
        }, {
            date: "2017-2018",
            title: "Backend 1C-Bitrix",
            content: "Passed Junior and Administrator certifications, worked for about seven months until lost interest to 1C-Bitrix.",
        }, {
            date: "2017",
            title: "SEO",
            content: "On freelance, was fulfilling orders for SEO optimization.",
        }, {
            date: "2016-2017",
            title: "System administration",
            content: "Was helping outsource system administrator, still doing orders with PHP at the same time.",
        }, {
            date: "2014",
            title: "Began to study PHP",
            content: "Was fulfilling small orders, created few admin-panels, feedback forms etc.",
        },
    ],
}