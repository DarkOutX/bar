// import './sprites.js';
import '../public/styles/styles.scss';

function buildCocktailBlock(cocktail) {
    /* 
    <div class="cocktail-wrapper">
        <div class="name">Самый Белый русский</div>
        <div class="dots"></div>
        <div class="price">150</div>
    </div>
     */
    let item_div = document.createElement("div"),
        item_rowDiv = document.createElement("div"),
        item_subDiv = document.createElement("div"),
        item_span = document.createElement("span"),
        span_text = [],
        total_ml = 0;
    
    for(let drink in cocktail.ingridients) {
        span_text.push(bar[drink].name);
        total_ml += cocktail.ingridients[drink];
    }
    
    item_div.className = "cocktail-wrapper";
    item_div.title = cocktail.tooltip;

    item_subDiv.className = "name";
    item_subDiv.innerText = cocktail.name;
    item_rowDiv.appendChild(item_subDiv);

    item_subDiv = document.createElement("div");
    item_subDiv.className = "ml";
    item_subDiv.innerText = (total_ml % 5 <= 0)?total_ml - (total_ml % 5):total_ml + (5 - total_ml % 5);
    // item_subDiv.innerText += "/ " + total_ml;
    item_rowDiv.appendChild(item_subDiv);

    item_subDiv = document.createElement("div");
    item_subDiv.className = "dots";
    item_rowDiv.appendChild(item_subDiv);

    item_subDiv = document.createElement("div");
    item_subDiv.className = "price";
    item_subDiv.innerText = Math.ceil(cocktail.price);
    item_rowDiv.appendChild(item_subDiv);
    
    item_div.appendChild(item_rowDiv);

    item_rowDiv = document.createElement("div");
    
    item_span.innerHTML = span_text.join(", ");
    item_rowDiv.appendChild(item_span);
    item_div.appendChild(item_rowDiv);

    return item_div;
}

function main() {
    cocktails.forEach(cocktail => {
        document.querySelector(`.wrapper__${cocktail.type}s`).appendChild(buildCocktailBlock(cocktail));
    });

    // document.querySelector("link[type='text/css']").href="1"
}
window.onload = main;